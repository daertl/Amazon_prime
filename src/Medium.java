import java.util.Date;

public class Medium {

	private String titel;
	private String kommentar;
	private Date erscheinungsdatum;
	private Genre genre;
	
	public Medium(String titel, String kommentar, Date erscheinungsdatum, Genre genre) {
		super();
		this.titel = titel;
		this.kommentar = kommentar;
		this.erscheinungsdatum = erscheinungsdatum;
		this.genre = genre;
	}
	
	@Override
	public String toString() {
		return "Medium [titel=" + titel + ", kommentar=" + kommentar + ", erscheinungsdatum=" + erscheinungsdatum
				+ ", genre=" + genre + "]";
	}

	public String getTitel() {
		return titel;
	}
	
	public void setTitel(String titel) {
		this.titel = titel;
	}
	
	public String getKommentar() {
		return kommentar;
	}
	
	public void setKommentar(String kommentar) {
		this.kommentar = kommentar;
	}
	
	public Date getErscheinungsdatum() {
		return erscheinungsdatum;
	}
	
	public void setErscheinungsdatum(Date erscheinungsdatum) {
		this.erscheinungsdatum = erscheinungsdatum;
	}
	
	public Genre getGenre() {
		return genre;
	}
	
	public void setGenre(Genre genre) {
		this.genre = genre;
	}
	
	

}
