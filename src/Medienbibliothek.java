import java.util.ArrayList;
import java.util.Iterator;

public class Medienbibliothek {

	ArrayList<Medium> mb;
	
	public Medienbibliothek() {
		mb = new ArrayList<>();
	}
	
	public void printAllMedia(){
		for(Medium m : mb){
			System.out.println(m.toString());
		}
	}
	
	public void printMedia(String titel){
		boolean gefunden = false;
		for(Medium m: mb){
			if(titel.equals(m.getTitel())){
				System.out.println(m.toString());
				gefunden = true;	
			}
		}
		if(gefunden == false){
			System.out.println("Kein Medium gefunden!");
		}
	}
	
	public void addMedium(Medium medium){
		mb.add(medium);
	}
	
	public void removeMedium(Medium medium){
		mb.remove(medium);
	}
	
	public void removeMedium(String titel){
		boolean gefunden = false;
		Iterator<Medium> it = mb.iterator();
		
		while(it.hasNext()){
			Medium m = it.next();
			if(titel.equals(m.getTitel())){
				it.remove();
				gefunden = true;	
			}
		}
		if(gefunden == false){
			System.out.println("Kein Medium gefunden!");
		}
	}

}
