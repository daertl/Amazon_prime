import java.util.Date;
import java.util.GregorianCalendar;

public class App {

	public static void main(String[] args) {
		
		GregorianCalendar cal = new GregorianCalendar(2016,8,5);
		Date date = cal.getTime();
		Genre genre1 = new Genre("Fantasy");
		Urheber autor = new Urheber("Oliver","Guder","OG");
		Medium m1 = new eBook("Goblin","super",date, genre1 ,1524574,400,8,autor);
		Medium m2 = new Film("Harry Potter","ich bin keine Eule",date,genre1,autor,120,true,false);
		
		System.out.println(m1.toString());
		
		System.out.println(m2.toString());
		Medienbibliothek mb = new Medienbibliothek();
		
		mb.addMedium(m1);
		mb.addMedium(m2);
		System.out.println("--------------------------------------------------------------------");
		mb.printAllMedia();
		System.out.println("--------------------------------------------------------------------");
		mb.printMedia(m1.getTitel());
		System.out.println("--------------------------------------------------------------------");
		mb.printMedia(m2.getTitel());
		System.out.println("--------------------------------------------------------------------");
		mb.removeMedium(m1);
		mb.printAllMedia();
		
	}

}
