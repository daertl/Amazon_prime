import java.util.Date;

public class eBook extends Medium{

	private int isbn;
	private int seitenAnzahl;
	private int auflage;
	private Urheber autor;
	
	public eBook(String titel, String kommentar, Date erscheinungsdatum, Genre genre, int isbn, int seitenAnzahl, int auflage, Urheber autor) {
		super(titel, kommentar, erscheinungsdatum,genre);
		this.isbn = isbn;
		this.seitenAnzahl = seitenAnzahl;
		this.auflage = auflage;
		this.autor = autor;
	}
	
	public int getIsbn() {
		return isbn;
	}
	public void setIsbn(int isbn) {
		this.isbn = isbn;
	}
	public int getSeitenAnzahl() {
		return seitenAnzahl;
	}
	public void setSeitenAnzahl(int seitenAnzahl) {
		this.seitenAnzahl = seitenAnzahl;
	}
	public int getAuflage() {
		return auflage;
	}
	public void setAuflage(int auflage) {
		this.auflage = auflage;
	}
	public Urheber getAutor() {
		return autor;
	}
	public void setAutor(Urheber autor) {
		this.autor = autor;
	}
	
	@Override
	public String toString() {
		
		return super.toString()+ "\n eBook [isbn=" + isbn + ", seitenAnzahl=" + seitenAnzahl + ", auflage=" + auflage + ", autor=" + autor
				+ "]";
	}
	

}
