import java.util.Date;

public class Film extends Medium{
	
	private Urheber regisseur;
	private int spielZeit;
	private boolean hd;
	private boolean uhd;


	public Film(String titel, String kommentar, Date erscheinungsdatum, Genre genre, Urheber regisseur, int spielZeit, boolean hd, boolean uhd) {
		super(titel, kommentar, erscheinungsdatum,genre);
		this.regisseur = regisseur;
		this.spielZeit = spielZeit;
		this.hd = hd;
		this.uhd = uhd;
	}
	
	public Urheber getRegisseur() {
		return regisseur;
	}

	public void setRegisseur(Urheber regisseur) {
		this.regisseur = regisseur;
	}

	public int getSpielZeit() {
		return spielZeit;
	}

	public void setSpielZeit(int spielZeit) {
		this.spielZeit = spielZeit;
	}

	public boolean isHd() {
		return hd;
	}

	public void setHd(boolean hd) {
		this.hd = hd;
	}

	public boolean isUhd() {
		return uhd;
	}

	public void setUhd(boolean uhd) {
		this.uhd = uhd;
	}

	@Override
	public String toString() {
		
		return super.toString()+"\n Film [regisseur=" + regisseur + ", spielZeit=" + spielZeit + ", hd=" + hd + ", uhd=" + uhd + "]";
	}
	
	


}
